//
//  AaAppUtils.m
//  VIVE Targi Kielce
//
//  Created by Jacek Kwiecień on 13.08.2013.
//  Copyright (c) 2013 Jacek Kwiecień. All rights reserved.
//

#import "AaAppUtils.h"
#import "AFNetworking.h"

@implementation AaAppUtils

+ (BOOL)isiOSAtLeastVersion:(NSString *)requiredVersion
{
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    
    if ([currSysVer compare:requiredVersion options:NSNumericSearch] != NSOrderedAscending) return YES;
    else return NO;
}

+(NSInteger)getStatusCode:(NSError *)error {
    return [[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
}

+(BOOL)isAppInForeground
{
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    return !(state == UIApplicationStateBackground || state == UIApplicationStateInactive);
}


@end
