//
//  AprilAppsDateTools.m
//  nPP Lux-Med
//
//  Created by Jacek Kwiecień on 03.07.2013.
//  Copyright (c) 2013 Lux-Med. All rights reserved.
//

#import "AaDateTools.h"

@implementation AaDateTools

+(NSDate*) addDays:(NSInteger)days toDate:(NSDate*)date
{
    return [NSDate dateWithTimeInterval:60*60*24*days sinceDate:date];
}

+(NSDate*) addYears:(NSInteger)years toDate:(NSDate*)date
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *addComponents = [[NSDateComponents alloc] init];
    addComponents.year = + years;
    
    return [calendar dateByAddingComponents:addComponents toDate:date options:0];
}

+(NSDate*) minusDays:(NSInteger)days fromDate:(NSDate*)date
{
    return [NSDate dateWithTimeInterval:-60*60*24*days sinceDate:date];
}

+(NSDate *)dateFromString:(NSString *)string withPattern:(NSString *)pattern
{
    NSDateFormatter * dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:pattern];
    return [dateFormat dateFromString:string];
}

+(NSString *)stringFromDate:(NSDate *)date withPattern:(NSString *)pattern
{
    NSDateFormatter * dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:pattern];
    return [dateFormat stringFromDate:date];
}

+(NSInteger)daysBetweenDate:(NSDate *)dateFrom andDate:(NSDate *)dateTo
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSDayCalendarUnit fromDate:dateFrom toDate:dateTo options:0];
    return components.day;
}

+(NSDate*)addHours:(NSInteger)hoursToadd toDate:(NSDate*)date
{
    NSTimeInterval seconds = hoursToadd * 60 * 60;
    NSDate *newDate = [date dateByAddingTimeInterval:seconds];
    return newDate;
}

+(NSDate*)addSeconds:(NSInteger)secondsToadd toDate:(NSDate*)date
{    
    NSDate *newDate = [date dateByAddingTimeInterval:secondsToadd];
    return newDate;
}

+(NSDate*) dateWithSpecifiedHour:(NSInteger)hour ofDay:(NSDate*)date
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorian components:NSCalendarCalendarUnit|NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit|NSMinuteCalendarUnit|NSHourCalendarUnit|NSSecondCalendarUnit fromDate:date];
    [comps setHour:hour];
    [comps setMinute:0];
    [comps setSecond:0];

    NSDate *resultDate = [gregorian dateFromComponents:comps];
    
    return resultDate;
}

+(NSDate*)lastSecondOfADay:(NSDate*)date
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorian components:NSCalendarCalendarUnit|NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit|NSMinuteCalendarUnit|NSHourCalendarUnit|NSSecondCalendarUnit fromDate:date];
    [comps setHour:23];
    [comps setMinute:59];
    [comps setSecond:59];
    
    NSDate *resultDate = [gregorian dateFromComponents:comps];
    
    return resultDate;
}

+(NSDate *)plusMinutes:(NSInteger)minutes toDate:(NSDate *)date
{
    return [date dateByAddingTimeInterval:minutes*60];
}

+(NSDate *)minusMinutes:(NSInteger)minutes toDate:(NSDate *)date
{
    return [date dateByAddingTimeInterval:-minutes*60];
}

+(NSMutableArray *)calendarCardWithDateInMonth:(NSDate *)date
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSDateComponents* dateFromComponents = [calendar components:NSYearForWeekOfYearCalendarUnit |NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:date];
    
    NSDateComponents* firstDayDateComponent = [calendar components:NSYearForWeekOfYearCalendarUnit |NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:date];
    [firstDayDateComponent setDay:1];
    NSDate *firstDayDate = [calendar dateFromComponents:firstDayDateComponent];
    NSDateComponents* checkFirstDayDateComponent = [calendar components:NSYearForWeekOfYearCalendarUnit |NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:firstDayDate];
    long weekdayOrdinal = 0;
    if (checkFirstDayDateComponent.weekday == 2) {
        weekdayOrdinal = 1;
    }
    
    [dateFromComponents setHour:12];
    [dateFromComponents setWeekdayOrdinal:weekdayOrdinal];
    [dateFromComponents setWeekday:2];
    NSDate *dateFrom = [calendar dateFromComponents:dateFromComponents];
    
    NSDate *toArrayDate = dateFrom;
    NSMutableArray *daysArray = [NSMutableArray array];
    NSDateComponents *addOneDayComponents = [NSDateComponents new];
    [addOneDayComponents setDay:1];
    
    NSDate *lastMonthDay = [self endOfTheMonthWithDateInMonth:date];
    NSDateComponents *lastMonthDateComp = [calendar components:NSYearForWeekOfYearCalendarUnit |NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit fromDate:lastMonthDay];
    lastMonthDateComp.hour = 12; lastMonthDateComp.minute = 0; lastMonthDateComp.second = 0;
    lastMonthDay = [calendar dateFromComponents:lastMonthDateComp];
    while (toArrayDate) {
        [daysArray addObject:toArrayDate];
        NSDate *tempDate = [calendar dateByAddingComponents:addOneDayComponents toDate:toArrayDate options:0];
        NSDateComponents *comp = [calendar components:NSWeekdayCalendarUnit fromDate:tempDate];
        if ((comp.weekday == 2)&&([tempDate timeIntervalSinceDate:lastMonthDay] >= 0)) {
            toArrayDate = nil;
        }else{
            toArrayDate = tempDate;
        }
    }
    return daysArray;
}

+(NSMutableArray *)calendarCardWithMonth:(NSInteger)month andYear:(NSInteger)year
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    return [self calendarCardWithDateInMonth:[dateFormatter dateFromString: [NSString stringWithFormat:@"%li-%li-01 00:00:00 UTC",(long)year,(long)month]]];
}

// Zwraca pierwszy dzien miesiąca w zamian za dowolny dzien tego miesiąca
+(NSDate *)startOfTheMonthWithDateInMonth:(NSDate *)month
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* firstDayDateComponent = [calendar components:NSYearForWeekOfYearCalendarUnit |NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:month];
    [firstDayDateComponent setDay:1];
    [firstDayDateComponent setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    return [calendar dateFromComponents:firstDayDateComponent];
}
// Zwraca pierwszy dzien miesiąca w zamian za dowolny dzien tego miesiąca (miesiac i rok jako int)
+(NSDate *)startOfTheMonthWithMonth:(NSInteger)month andYear:(NSInteger)year
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    return [self startOfTheMonthWithDateInMonth:[dateFormatter dateFromString: [NSString stringWithFormat:@"%li-%li-01 00:00:00 UTC",(long)year,(long)month]]];
}

// Zwraca ostatni dzien miesiąca w zamian za dowolny dzien tego miesiąca
+(NSDate *)endOfTheMonthWithDateInMonth:(NSDate *)date
{
    NSCalendar *calendar=[NSCalendar currentCalendar];
    NSDateComponents *components=[calendar components:NSMonthCalendarUnit|NSYearCalendarUnit fromDate:date];
    NSInteger month=[components month];
    NSInteger year=[components year];
    if (month==12) {
        [components setYear:year+1];
        [components setMonth:1];
    }
    else {
        [components setMonth:month+1];
    }
    [components setDay:1];
    [components setSecond:-1];
    [components setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    return [[calendar dateFromComponents:components] dateByAddingTimeInterval:0];
}
// Zwraca pierwszy dzien miesiąca w zamian za dowolny dzien tego miesiąca (miesiac i rok jako int)
+(NSDate *)endOfTheMonthWithMonth:(NSInteger)month andYear:(NSInteger)year
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    return [self endOfTheMonthWithDateInMonth:[dateFormatter dateFromString: [NSString stringWithFormat:@"%li-%li-01 00:00:00 UTC",(long)year,(long)month]]];
}

// Sprawdza czy podany dzien jest w tym miesiacu
+(BOOL)isDateWithinCurrentMonth:(NSDate*)date
{
    NSDate *firstCurrent = [self startOfTheMonthWithDateInMonth:[NSDate date]];
    NSDate *firstDate = [self startOfTheMonthWithDateInMonth:date];
    return ![firstCurrent compare:firstDate];
}

+(BOOL)isDate:(NSDate *)date withinMonthFromDate:(NSDate *)withinMonthFromDate
{
    NSDate *firstWithinMonthFromDate = [self startOfTheMonthWithDateInMonth:withinMonthFromDate];
    NSDate *firstDate = [self startOfTheMonthWithDateInMonth:date];
    return ![firstWithinMonthFromDate compare:firstDate];
}

+(BOOL)isDate:(NSDate *)date withinMonth:(NSInteger)month andYear:(NSInteger)year
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    return [self isDate:date withinMonthFromDate:[dateFormatter dateFromString: [NSString stringWithFormat:@"%li-%li-01 00:00:00 UTC",(long)year,(long)month]]];
}

//Funkcja po przyjęciu liczby sekund przelicza to na godziny, minuty i sekundy
//Zwraca NSDateComponents
+(NSDateComponents *)timeComponentsFromSeconds:(NSInteger)seconds
{
    long lHours,lMinutes,lSeconds;
    long lAllSeconds = seconds;
    
    lHours = lAllSeconds / (60 * 60); //ilość godzin
    lAllSeconds -= lHours * (60 * 60);//odejmujemy sekundy z godzinami
    lMinutes = lAllSeconds / 60;      //liczba minut
    lAllSeconds -= lMinutes * 60;     //odejmujemy sekundy z minutami
    lSeconds = lAllSeconds;           //pozostałe sekundy
    
    
    NSDateComponents *comp = [NSDateComponents new];
    [comp setHour:lHours];
    [comp setMinute:lMinutes];
    [comp setSecond:lSeconds];
    return comp;
}

+(NSArray *)monthsArrayFirstUpper
{
    return [NSArray arrayWithObjects:NSLocalizedString(@"January", nil), NSLocalizedString(@"February", nil), NSLocalizedString(@"March", nil), NSLocalizedString(@"April", nil), NSLocalizedString(@"May", nil), NSLocalizedString(@"June", nil), NSLocalizedString(@"July", nil), NSLocalizedString(@"August", nil),
        NSLocalizedString(@"September", nil), NSLocalizedString(@"October", nil), NSLocalizedString(@"November", nil), NSLocalizedString(@"December", nil), nil];
}
@end
