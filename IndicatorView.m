//
//  IndicatorView.m
//  FreeBee
//
//  Created by Łukasz Walukiewicz on 11-12-12.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "IndicatorView.h"
#import "AppDelegate.h"

@implementation IndicatorView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        self.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5];
        _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _activityIndicator.center = self.center;
        [self addSubview:_activityIndicator];
        [_activityIndicator startAnimating];
    }
    return self;
    
}

- (id)initWithFrame:(CGRect)frame andAlpha:(float)alpha
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:alpha];
        _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _activityIndicator.center = self.center;
        [self addSubview:_activityIndicator];
        [_activityIndicator startAnimating];
    }
    return self;
    
}

- (void)setActivityIndicatorCenter:(CGPoint)center
{
    
    _activityIndicator.center = center;
    
}


+(IndicatorView*) addToViewControlle:(UIViewController *)viewController{    
    
    IndicatorView *view = [[IndicatorView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    AppDelegate *appDelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];    
    [appDelegate.window performSelectorOnMainThread:@selector(addSubview:) withObject:view waitUntilDone:YES];
   // [appDelegate.window addSubview:view];
    [appDelegate.window bringSubviewToFront:view]; 
    
    return view;
}

+(IndicatorView*) addToViewControlle:(UIViewController *)viewController withText:(NSString *)text
{
    IndicatorView *view = [[IndicatorView alloc] initWithFrame:CGRectMake(0, 0, 320, 480) andAlpha:0.8];
    AppDelegate *appDelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    [appDelegate.window performSelectorOnMainThread:@selector(addSubview:) withObject:view waitUntilDone:YES];
    // [appDelegate.window addSubview:view];
    
    
   
    UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 340)];
    [infoLabel setTextColor:[UIColor whiteColor]];
    [infoLabel setFont:[UIFont fontWithName: @"Trebuchet MS" size: 20.0f]];
    [infoLabel setText:text];
    infoLabel.textAlignment = NSTextAlignmentCenter;
    infoLabel.numberOfLines = 4;
    [view addSubview:infoLabel];
    
    [appDelegate.window bringSubviewToFront:view];
    
    return view;
}

-(void)removeFromSuperview{
    
    [super removeFromSuperview];
//    [controller.navigationController.navigationBar setUserInteractionEnabled:YES];
//    controller = nil;
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
