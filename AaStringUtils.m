//
//  AaStringUtils.m
//  Owl Money Manager
//
//  Created by Jacek Kwiecień on 20.10.2013.
//  Copyright (c) 2013 AprilApps. All rights reserved.
//

#import "AaStringUtils.h"

@implementation AaStringUtils


+(BOOL)string:(NSString *)string containsString:(NSString *)partialString caseSensitive:(BOOL)caseSensitive{
    if (caseSensitive) {
        if ([string rangeOfString:partialString].location == NSNotFound) {
            return NO;
        } else {
            return YES;
        }
    } else {
        if ([string rangeOfString:partialString options:NSCaseInsensitiveSearch].location == NSNotFound) {
            return NO;
        } else {
            return YES;
        }
    }
}

+(BOOL) isEmailValid:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+(NSString *)hexToString:(NSData *)hexData
{
    return [[NSString alloc] initWithData:hexData encoding:NSUTF8StringEncoding];
}

+(BOOL)isAlphanumericString:(NSString *)string
{
    NSString *filter = @"[a-z0-9]*";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", filter];
    BOOL result =  [predicate evaluateWithObject:string];
    return result;
}


@end
