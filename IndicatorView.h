//
//  IndicatorView.h
//  FreeBee
//
//  Created by Łukasz Walukiewicz on 11-12-12.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Class used to show progress of operation. 
 This is the view with spinner which is added as subview to UIWindow to cover TabBar
 and NavigationController.
 */
@interface IndicatorView : UIView
{
    UIActivityIndicatorView *_activityIndicator;
    
    UIViewController * controller;
    
}

- (void)setActivityIndicatorCenter:(CGPoint)center;


+(IndicatorView*) addToViewControlle:(UIViewController *)viewController;
+(IndicatorView*) addToViewControlle:(UIViewController *)viewController withText:(NSString*)text;
-(void)removeFromSuperview;



@end
