//
//  AaNumberTools.h
//  Transfer24
//
//  Created by Jacek Kwiecień on 30.10.2013.
//  Copyright (c) 2013 Hubert Bysiak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AaNumberTools : NSObject

+(NSString*)currencyStringWithNumber:(NSNumber*)value;
+(NSNumber*)numberWithCurrencyString:(NSString*)valueString;
+(NSString *)moneyStringFromValue:(NSNumber *)value withCurrencyCode:(NSString *)code andCurrencySymbol:(NSString *)symbol;
+(NSString *)moneyStringWithoutCurrencyFromValue:(NSNumber *)value;
+(NSString *)moneyStringFromValue:(NSNumber *)value withCurrencySymbolOnTheRight:(NSString*)currencySymbol;
+(NSNumber *)numberWithMoneyString:(NSString *)stringValue;
+(NSArray *)timeComponentsFromSeconds:(NSNumber *)seconds;

+(NSNumber *)kilometersPerHourFromMetersPerSecond:(NSNumber *)mps;
+(NSNumber *)minutesPerKilometerFromMetersPerSecond:(NSNumber *)mps;
+(NSString*)decimalSeparatorForSystemLanguage;
+(NSString*)replaceSeparatorWithCurrentLocale:(NSString*)numberString;
@end
