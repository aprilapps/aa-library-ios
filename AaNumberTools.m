//
//  AaNumberTools.m
//  Transfer24
//
//  Created by Jacek Kwiecień on 30.10.2013.
//  Copyright (c) 2013 Hubert Bysiak. All rights reserved.
//

#import "AaNumberTools.h"

@implementation AaNumberTools

+(NSNumber *)numberWithCurrencyString:(NSString *)valueString
{
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber * value = [f numberFromString:valueString];
    return value;
}

+(NSString *)currencyStringWithNumber:(NSNumber *)value
{
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSString * valueString = [f stringFromNumber:value];
    return valueString;
}

+(NSString *)moneyStringFromValue:(NSNumber *)value withCurrencyCode:(NSString *)code andCurrencySymbol:(NSString *)symbol
{
    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    [nf setCurrencyCode:code];
    [nf setCurrencySymbol:symbol];
    [nf setNumberStyle: NSNumberFormatterCurrencyStyle];
    NSString *stringValue = [nf stringFromNumber:value];
    return stringValue;
}

+(NSString *)moneyStringWithoutCurrencyFromValue:(NSNumber *)value
{
    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    [nf setPositiveFormat:@"#,##0.00"];
    [nf setNegativeFormat:@"-#,##0.00"];
    [nf setNumberStyle: NSNumberFormatterCurrencyStyle];
    NSString *stringValue = [nf stringFromNumber:value];
    return stringValue;
}

+(NSNumber *)numberWithMoneyString:(NSString *)stringValue
{
    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    [nf setPositiveFormat:@"#,##0.00"];
    [nf setNegativeFormat:@"-#,##0.00"];
    [nf setNumberStyle: NSNumberFormatterCurrencyStyle];
    return [nf numberFromString:stringValue];
}

+(NSString *)moneyStringFromValue:(NSNumber *)value withCurrencySymbolOnTheRight:(NSString*)currencySymbol
{
    return [NSString stringWithFormat:@"%@ %@", [AaNumberTools moneyStringWithoutCurrencyFromValue:value], currencySymbol];
}

//Funkcja po przyjęciu liczby sekund przelicza to na godziny, minuty i sekundy
//Zwraca NSArray gdzie [0] ma godziny, [1] ma minuty, [2] ma sekundy
+(NSArray *)timeComponentsFromSeconds:(NSNumber *)seconds
{
    long lHours,lMinutes,lSeconds;
    long lAllSeconds = [seconds longValue];
    
    lHours = lAllSeconds / (60 * 60); //ilość godzin
    lAllSeconds -= lHours * (60 * 60);//odejmujemy sekundy z godzinami
    lMinutes = lAllSeconds / 60;      //liczba minut
    lAllSeconds -= lMinutes * 60;     //odejmujemy sekundy z minutami
    lSeconds = lAllSeconds;           //pozostałe sekundy
    
    return @[[NSNumber numberWithLong:lHours],[NSNumber numberWithLong:lMinutes],[NSNumber numberWithLong:lSeconds]];
}

+(NSNumber *)kilometersPerHourFromMetersPerSecond:(NSNumber *)mps
{
    if (!mps || mps.floatValue == 0) return [NSNumber numberWithFloat:0.0];
    else {
        NSNumber *speed = [NSNumber numberWithDouble:[mps doubleValue] * 3.6];
        return speed;
    }
}

+(NSNumber *)minutesPerKilometerFromMetersPerSecond:(NSNumber *)mps
{
    if (!mps || mps.doubleValue == 0.0) return [NSNumber numberWithFloat:0.0];
    NSNumber *kph = [self kilometersPerHourFromMetersPerSecond:mps];
    NSNumber *result = [NSNumber numberWithDouble: 3600.0/[kph doubleValue]/60.0];
    return result;
}

+(NSString *)decimalSeparatorForSystemLanguage
{
    return [[NSLocale currentLocale] objectForKey:NSLocaleDecimalSeparator];
}

+(NSString *)replaceSeparatorWithCurrentLocale:(NSString *)numberString
{
    return [numberString stringByReplacingOccurrencesOfString:@"." withString:[AaNumberTools decimalSeparatorForSystemLanguage]];
}
@end
