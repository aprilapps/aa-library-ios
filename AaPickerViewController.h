//
//  AaPickerViewController.h
//  Transfer24
//
//  Created by Jacek Kwiecień on 24.10.2013.
//  Copyright (c) 2013 Hubert Bysiak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AaBasePickerViewController.h"

@interface AaPickerViewController : AaBasePickerViewController
{
    NSMutableArray *objects;
    NSInteger selectedRow;
    UIPickerView *picker;
}

@property (nonatomic, retain) NSMutableArray *objects;
@property (nonatomic) NSInteger selectedRow;
@property (nonatomic, retain) IBOutlet UIPickerView *picker;

@end
