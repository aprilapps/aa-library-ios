//
//  AaStringUtils.h
//  Owl Money Manager
//
//  Created by Jacek Kwiecień on 20.10.2013.
//  Copyright (c) 2013 AprilApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AaStringUtils : NSObject

+(BOOL)string:(NSString*)string containsString:(NSString*)partialString caseSensitive:(BOOL)caseSensitive;
+(BOOL) isEmailValid:(NSString *)checkString;
+(NSString *)hexToString:(NSData *)hexData;
+(BOOL)isAlphanumericString:(NSString*)string;
@end
